/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Light
 * @{
 *
 * @brief Provides APIs for the light service to access the light driver.
 *
 * 
 * After obtaining the light driver object or proxy, the light service can call related APIs to obtain light information, turn on or off lights, and set the light blinking mode based on the light type.
 *
 * @since 3.1
 * @version 1.0
 */

/**
 * @file Light_if.h
 *
 * @brief Defines light data structures, including the light status, light ID, lighting mode, blinking parameters, color model, lighting effect, and basic light information.
 *
 * @since 3.1
 * @version 1.0
 */

#ifndef LIGHT_TYPE_H
#define LIGHT_TYPE_H

#include <stdint.h>

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/**
 * @brief Enumerates the light statuses.
 *
 * @since 3.1
 * @version 1.0
 */
enum LightStatus {
    /** The operation is successful. */
    LIGHT_SUCCESS            = 0,
    /** The light type is not supported. */
    LIGHT_NOT_SUPPORT        = -1,
    /** Blinking setting is not supported. */
    LIGHT_NOT_FLASH          = -2,
    /** Light brightness setting is not supported. */
    LIGHT_NOT_BRIGHTNESS     = -3,
};

/**
 * @brief Enumerates the light types.
 *
 * @since 3.1
 * @version 1.0
 */
enum LightId {
    /** Unknown type. */
    LIGHT_ID_NONE                = 0,
    /** Power light. */
    LIGHT_ID_BATTERY             = 1,
    /** Notification light. */
    LIGHT_ID_NOTIFICATIONS       = 2,
    /** Alarm light. */
    LIGHT_ID_ATTENTION           = 3,
    /** Invalid type. */
    LIGHT_ID_BUTT                = 4,
};

/**
 * @brief Enumerates the lighting modes.
 *
 * @since 3.1
 * @version 1.0
 */
enum LightFlashMode {
    /** Steady on. */
    LIGHT_FLASH_NONE     = 0,
    /** Blinking. */
    LIGHT_FLASH_TIMED    = 1,
    /** Gradient. */
    LIGHT_FLASH_GRADIENT = 2,
    /** Invalid mode */
    LIGHT_FLASH_BUTT     = 3,
};

/**
 * @brief Defines the blinking parameters, including the blinking mode and the on and off time of the light during the blinking period.
 *
 * 
 *
 * @since 3.1
 * @version 1.0
 */
struct LightFlashEffect {
    /** Blinking mode. For details, see {@link LightFlashMode}. */
    int32_t flashMode;
    /** Duration (in ms) for which the light remains on during the blinking period. */
    int32_t onTime;
    /**Duration (in ms) for which the light remains off during the blinking period. */
    int32_t offTime;
};

/**
 * @brief Defines the red green blue (RGB) color model.
 *
 * @since 3.2
 * @version 1.0
 */
struct RGBColor {
    /** Brightness value, which ranges from 0 to 255. */
    int brightness;
    /** Red value, which ranges from 0 to 255. */
    int r;
    /** Green value, which ranges from 0 to 255. */
    int g;
    /** Blue value, which ranges from 0 to 255. */
    int b;
};

/**
 * @brief Defines the white red green blue (WRGB) color model.
 *
 * @since 3.2
 * @version 1.0
 */
struct WRGBColor {
    /** White value, which ranges from 0 to 255. */
    int w;
    /** Red value, which ranges from 0 to 255. */
    int r;
    /** Green value, which ranges from 0 to 255. */
    int g;
    /** Blue value, which ranges from 0 to 255. */
    int b;
};

/**
 * @brief Defines the color model of a light.
 *
 * 
 *
 * @since 3.2
 * @version 1.0
 */
union ColorValue {
    /** WRGB color model. For details, see {@link WRGBColor}. */
    struct WRGBColor wrgbColor;
    /** RGB color model. For details, see {@link RGBColor}. */
    struct RGBColor rgbColor;
};

/**
 * @brief Defines the lighting parameters, including the light color model.
 *
 * 
 *
 * @since 3.2
 * @version 1.0
 */
struct LightColor {
    /** Light color model. For details, see {@link ColorValue}. */
    union ColorValue colorValue;
};

/**
 * @brief Defines the lighting effect parameters, including the brightness and blinking mode.
 *
 * 
 *
 * @since 3.1
 * @version 1.0
 */
struct LightEffect {
    /**
     * Light brightness. Bits 0 to 7 stands for blue, bits 8 to 15 for green, bits 16 to 23 for red, and bits 24 to 31 are reserved.
     * If the corresponding segment is not 0, the light of the corresponding color is turned on.
    */
    int32_t lightBrightness;
    /** Blinking mode. For details, see {@link LightFlashEffect}. */
    struct LightFlashEffect flashEffect;
};

/**
 * @brief Defines basic light information, including the light type, number of physical lights in the logical controller, logical light name, and custom information.
 *
 * 
 *
 * @since 3.1
 * @version 1.0
 */
struct LightInfo {
    /** Light type. For details, see {@link LightId}. */
    uint32_t lightId;
    /** Number of physical lights in the logical controller. */
    uint32_t lightNumber;
    /** Name of the logical light. */
    char lightName[NAME_MAX_LEN];
    /** Custom information. */
    int32_t reserved;
};

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* LIGHT_TYPE_H */
/** @} */
