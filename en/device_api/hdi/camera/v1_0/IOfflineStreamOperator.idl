/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Provides APIs for the camera module.
 *
 * You can use the APIs to perform operations on camera devices and streams (including offline streams),
 * and implement various callbacks.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IOfflineStreamOperator.idl
 *
 * @brief Declares APIs for offline stream operations.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the camera module APIs.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.camera.v1_0;

import ohos.hdi.camera.v1_0.Types;

/**
 * @brief Defines the offline stream operations available for camera devices.
 *
 * You can cancel a capture request and release offline streams.
 */
interface IOfflineStreamOperator {
     /**
     * @brief Cancels a capture request.
     *
     * @param captureId Indicates the ID of the capture request to cancel.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful.
     * @return Returns an error code defined in {@link CamRetCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    CancelCapture([in] int captureId);

     /**
     * @brief Releases specified offline streams.
     *
     * @param streamIds Indicates the IDs of the offline streams to release.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful.
     * @return Returns an error code defined in {@link CamRetCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    ReleaseStreams([in] int[] streamIds);

     /**
     * @brief Releases all offline streams.
     * The prerequisites for releasing all streams are as follows:
     * All single capture requests have been processed.
     * All consecutive capture requests have been canceled.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful.
     * @return Returns an error code defined in {@link CamRetCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    Release();
}
/** @} */
