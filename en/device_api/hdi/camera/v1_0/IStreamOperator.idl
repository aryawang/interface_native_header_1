/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Provides APIs for the camera module.
 *
 * You can use the APIs to perform operations on camera devices and streams (including offline streams),
 * and implement various callbacks.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IStreamOperator.idl
 *
 * @brief Declares APIs for stream operations.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the camera module APIs.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.camera.v1_0;

import ohos.hdi.camera.v1_0.Types;
import ohos.hdi.camera.v1_0.IStreamOperatorCallback;
import ohos.hdi.camera.v1_0.IOfflineStreamOperator;

sequenceable ohos.hdi.camera.v1_0.BufferProducerSequenceable;

/**
 * @brief Defines the stream operations available for camera devices.
 *
 * You can create, commit, convert, and release streams, obtain stream attributes, attach or detach a handle,
 * capture images, and cancel capture requests.
 *
 * A stream is a sequence of data elements output from a bottom-layer device, processed by the current module,
 * and then transmitted to an upper-layer service or application.
 * The current module supports preview streams, video streams, photographing streams, and the like.
 * For details, see {@link StreamIntent}.
 */
interface IStreamOperator {
    /**
     * @brief Checks whether a specific stream can be dynamically created.
     *
     * This function is used to check whether a stream can be dynamically created based on the operation mode, 
     * configuration information, and existing streams in the current module.
     * - If the stream can be created without stopping the existing streams or by stopping the existing streams
     *   while not being noticed by the upper-layer service or application, <b>type</b> set to <b>DYNAMIC_SUPPORTED</b>
     *   is returned so that the upper-layer service or application can directly add the new stream.
     * - If the stream can be created only after the upper-layer service or application stops capturing all streams,
     *   <b>type</b> set to <b>RE_CONFIGURED_REQUIRED</b> is returned.
     * - If creating the stream is not supported, <b>type</b> set to <b>NOT_SUPPORTED</b> is returned.
     * This function must be called prior to {@link CreateStreams}.
     *
     * @param mode Indicates the operation mode of the stream. For details, see {@link OperationMode}.
     * @param modeSetting Indicates the stream configuration parameters, including the frame rate and 3A. 
     * 3A stands for automatic focus (AF), automatic exposure (AE), and automatic white balance (AWB).
     * @param infos Indicates the stream configuration information. For details, see {@link StreamInfo}.
     * @param type Indicates the support type of the stream. The supported types are
     * defined in {@link StreamSupportType}.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful.
     * @return Returns an error code defined in {@link CamRetCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    IsStreamsSupported([in] enum OperationMode mode, [in] unsigned char[] modeSetting,
        [in] struct StreamInfo[] infos, [out] enum StreamSupportType type);

     /**
     * @brief Creates streams.
     *
     * Before calling this function, you must use {@link IsStreamsSupported} to check whether
     * the hardware abstraction layer (HAL) supports the streams to create.
     *
     * @param streamInfos Indicates the list of stream information, which is defined by {@link StreamInfo}.
     * The passed stream information may be changed.
     * Therefore, you can run {@link GetStreamAttributes} to obtain the latest stream attributes
     * after the streams are created.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful.
     * @return Returns an error code defined in {@link CamRetCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    CreateStreams([in] struct StreamInfo[] streamInfos);

     /**
     * @brief Releases streams.
     *
     * @param streamIds Indicates the IDs of the streams to release.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful.
     * @return Returns an error code defined in {@link CamRetCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    ReleaseStreams([in] int[] streamIds);

     /**
     * @brief Commits streams.
     *
     * This function can be called only after {@link CreateStreams}.
     *
     * @param mode Indicates the operation mode of the streams. For details, see {@link OperationMode}.
     * @param modeSetting Indicates the stream configuration parameters, including the frame rate and zoom information.  
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful.
     * @return Returns an error code defined in {@link CamRetCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    CommitStreams([in] enum OperationMode mode, [in] unsigned char[] modeSetting);

     /**
     * @brief Obtains stream attributes. 
     * Stream information passed by the <b>streamInfos</b> parameter in {@link CreateStreams} may be overridden.
     * Therefore, the stream attributes obtained by using this function may be 
     * different from the stream information passed in {@link CreateStreams}.
     *
     * @param attributes Indicates the obtained stream attributes.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful.
     * @return Returns an error code defined in {@link CamRetCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    GetStreamAttributes([out] struct StreamAttribute[] attributes);

     /**
     * @brief Attaches a producer handle to a stream.
     *
     * If a producer handle has been specified during stream creation (by {@link CreateStreams}), 
     * you do not need to call this function. If you want to attach a new procedure handle,
     * call {@link DetachBufferQueue} to detach the existing procedure handle and then
     * call this function to attach the new one.
     * You do not need to attach a procedure handle for IoT devices that do not support
      * or require image data caching and preview stream forwarding.
     * In this case, set <b>bufferQueue_</b> in the {@link StreamInfo} parameter of
     * {@link CreateStreams} to <b>null</b>, and set <b>tunneledMode_</b> to <b>false</b>.
     *
     * @param streamId Indicates the ID of the stream to which the procedure handle is to be attached.
     * @param bufferProducer Indicates the producer handle to be attached.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful.
     * @return Returns an error code defined in {@link CamRetCode} if the operation fails.
     *
     * @see DetachBufferQueue
     *
     * @since 3.2
     * @version 1.0
     */
    AttachBufferQueue([in] int streamId, [in] BufferProducerSequenceable bufferProducer);

     /**
     * @brief Detaches the producer handle from a stream.
     *
     * @param streamId Indicates the ID of the stream from which the procedure handle is to be detached.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful.
     * @return Returns an error code defined in {@link CamRetCode} if the operation fails.
     *
     * @see AttachBufferQueue
     *
     * @since 3.2
     * @version 1.0
     */
    DetachBufferQueue([in] int streamId);

     /**
     * @brief Captures an image.
     *
     * This function must be called after {@link CommitStreams}.
     * There are two image capture modes: continuous capture and single capture.
     * - After a continuous capture is triggered, frames of image data are captured continuously
     *   until this function is called again.
     *   Continuous capture applies to the preview, video recording, and burst shooting scenarios.
     * - After a single capture is triggered, only one frame of image data is captured. 
     *   Single capture applies to the single shooting scenario. 
     *   When the capture is started, {@link OnCaptureStarted} is called to notify of the start.
     * - To stop a continuous capture, call {@link CancelCapture}. When the capture ends,
     *   {@link OnCaptureEnded} is called to notify the caller of the information such as the number of captured frames.
     * <b>enableShutterCallback_</b> in {@link CaptureInfo} is used to enable the callback for each capture.
     * After the callback is enabled, {@link OnFrameShutter} is called upon each capture.
     * In the scenario where multiple streams are captured at the same time,
     * this module ensures that the captured data of multiple streams is reported simultaneously.
     *
     * @param captureId Indicates the ID of the capture request. You must ensure that the ID
     * of the capture request is unique when the camera is started.
     * @param info Indicates the capture request configuration information. For details, see {@link CaptureInfo}.
     * @param isStreaming Specifies whether to continuously capture images.
     * The value <b>true</b> means to continuously capture images, and <b>false</b> means the opposite.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful.
     * @return Returns an error code defined in {@link CamRetCode} if the operation fails.
     *
     * @see OnFrameShutter
     *
     * @since 3.2
     * @version 1.0
     */
    Capture([in] int captureId, [in] struct CaptureInfo info, [in] boolean isStreaming);

     /**
     * @brief Cancels a continuous capture. {@link OnCaptureEnded} is called when a continuous capture is canceled.
     *
     * @param captureId Indicates the ID of the capture request to cancel.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful.
     * @return Returns an error code defined in {@link CamRetCode} if the operation fails.
     *
     * @see Capture
     *
     * @since 3.2
     * @version 1.0
     */
    CancelCapture([in] int captureId);

     /**
     * @brief Converts streams to the offline stream.
     *
     * Only photographing streams can be converted into offline streams.
     * Due to the limited processing capability, some devices may spend a long period of time on algorithm processing
     * during photographing, causing the capture requests to stack in the module. 
     * Converting to an offline stream enables the bottom-layer device to close
     * and the offline stream to take over for subsequent processing.
     *
     * @param streamIds Indicates the IDs of the streams to be converted.
     * @param callbackObj Indicates the callback for conversion to the offline stream.
     * @param offlineOperator Indicates the offline stream after conversion.
     *
     * @return Returns <b>NO_ERROR</b> if the operation is successful.
     * @return Returns an error code defined in {@link CamRetCode} if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    ChangeToOfflineStream([in] int[] streamIds, [in] IStreamOperatorCallback callbackObj,
        [out] IOfflineStreamOperator offlineOperator);
}
/** @} */
