
/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Provides APIs for the camera module.
 *
 * You can use the APIs to perform operations on camera devices and streams (including offline streams),
 * and implement various callbacks.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file ICameraDeviceCallback.idl
 *
 * @brief Declares callbacks for reporting camera device errors and metadata.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the camera module APIs.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.camera.v1_0;

import ohos.hdi.camera.v1_0.Types;

/**
 * @brief Defines the callbacks for camera devices.
 *
 * You can set callbacks to obtain error information and metadata.
 */
[callback] interface ICameraDeviceCallback {

     /**
     * @brief Called when an error occurs on the camera device. The caller needs to implement this function.
     *
     * @param type Indicates the error type. For details, see {@link ErrorType}.
     * @param errorCode Indicates the error code. This parameter is reserved.
     *
     * @since 3.2
     * @version 1.0
     */
    OnError([in] enum ErrorType type, [in] int errorCode);

    /**
     * @brief Called to report metadata related to the camera device.
     * For details about the reporting mode, see {@link SetResultMode}.
     *
     * @param timestamp Indicates the timestamp when the metadata is reported.
     * @param result Indicates the metadata reported, which is specified by {@link EnableResult}.
     * You can call {@link GetEnabledResults} to obtain enabled metadata and
     * call {@link DisableResult} to disable metadata reporting.
     *
     * @since 3.2
     * @version 1.0
     */
    OnResult([in] unsigned long timestamp, [in] unsigned char[] result);
}
/** @} */
