/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_AVSESSION_CONTROLLER_H
#define OHOS_AVSESSION_CONTROLLER_H

/**
 * @addtogroup avsession
 * @{
 *
 * @brief Provides APIs for audio and video (AV) media control.
 *
 * The APIs can be used to create, manage, and control media sessions.
 *
 * @syscap SystemCapability.Multimedia.AVSession.Core
 * @since 9
 * @version 1.0
 */

/**
 * @file avsession_controller.h
 *
 * @brief Declares the controller object, which can be used to obtain the playback state and session metadata,
 * remotely send control commands to the session, and listen for session update events through subscription.
 *
 * @since 9
 * @version 1.0
 */

#include <bitset>
#include <memory>
#include <string>
#include <vector>

#include "avcontrol_command.h"
#include "avsession_info.h"
#include "key_event.h"
#include "want_agent.h"

namespace OHOS::AVSession {
/**
 * @brief Represents a session controller that provides APIs for obtaining the playback state and session metadata,
 * remotely sending control commands to the session, and listening for session update events through subscription.
 */
class AVSessionController {
public:

    /**
     * @brief Obtains the playback state.
     *
     * @param state Indicates the pointer to the playback state, which is an {@link AVPlaybackState} object.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @see AVSession#SetAVPlaybackState
     * @see AVSession#GetAVPlaybackState
     * @since 9
     * @version 1.0
     */
    virtual int32_t GetAVPlaybackState(AVPlaybackState &state) = 0;

    /**
     * @brief Obtains the session metadata.
     *
     * @param data Indicates the pointer to the session metadata, which is an {@link AVMetaData} object.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @see AVSession#SetAVMetaData
     * @see AVSession#GetAVMetaData
     * @since 9
     * @version 1.0
     */
    virtual int32_t GetAVMetaData(AVMetaData &data) = 0;

    /**
     * @brief Sends a system key event.
     *
     * @param keyEvent Indicates the pointer to the key event code, which is an {@link MMI::KeyEvent} object.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @see AVSessionManager#SendSystemAVKeyEvent
     * @since 9
     * @version 1.0
     */
    virtual int32_t SendAVKeyEvent(const MMI::KeyEvent& keyEvent) = 0;

    /**
     * @brief Obtains an ability.
     *
     * @param ability Indicates the pointer to the ability, which is an {@link AbilityRuntime::WantAgent::WantAgent} object.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @see AVSession#SetLaunchAbility
     * @since 9
     * @version 1.0
     */
    virtual int32_t GetLaunchAbility(AbilityRuntime::WantAgent::WantAgent &ability) = 0;

    /**
     * @brief Obtains valid control commands.
     *
     * @param cmds Indicates the pointer to the list of valid commands, 
     * ranging from {@link #SESSION_CMD_INVALID} to {@link #SESSION_CMD_MAX}.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @see SendControlCommand
     * @see AVSession#AddSupportCommand
     * @see AVSession#DeleteSupportCommand
     * @since 9
     * @version 1.0
     */
    virtual int32_t GetValidCommands(std::vector<int32_t> &cmds) = 0;

    /**
     * @brief Checks whether this session is activated.
     *
     * @param isActive Specifies whether the session is activated.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @see AVSession#Activate
     * @see AVSession#Deactivate
     * @see AVSession#IsActive
     * @since 9
     * @version 1.0
     */
    virtual int32_t IsSessionActive(bool &isActive) = 0;

    /**
     * @brief Sends a media control command.
     *
     * @param cmd Indicates the pointer to the control command to send, which is an {@link AVControlCommand} object.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @see GetValidCommands
     * @since 9
     * @version 1.0
     */
    virtual int32_t SendControlCommand(const AVControlCommand &cmd) = 0;

    /**
     * @brief Registers a callback.
     *
     * @param callback Indicates the pointer to the callback to register, which is an {@link AVControllerCallback} object.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @since 9
     * @version 1.0
     */
    virtual int32_t RegisterCallback(const std::shared_ptr<AVControllerCallback> &callback) = 0;

    /**
     * @brief Sets criteria for filtering session metadata.
     *
     * @param filter Indicates the pointer to the filter criteria to set, which is an {@link AVMetaData#MetaMaskType} object.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @since 9
     * @version 1.0
     */
    virtual int32_t SetMetaFilter(const AVMetaData::MetaMaskType &filter) = 0;

    /**
     * @brief Sets the criteria for filtering the returned playback state.
     *
     * @param filter Indicates the pointer to the filter criteria to set, 
     * which is an {@link AVPlaybackState#PlaybackStateMaskType} object.
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @since 9
     * @version 1.0
     */
    virtual int32_t SetPlaybackFilter(const AVPlaybackState::PlaybackStateMaskType &filter) = 0;

    /**
     * @brief Releases the controller.
     *
     * @return Returns {@link #AVSESSION_SUCCESS} if the operation is successful; returns an error code otherwise.
     * @since 9
     * @version 1.0
     */
    virtual int32_t Destroy() = 0;

    /**
     * @brief Obtains the session ID.
     *
     * @return Returns the session ID.
     * @since 9
     * @version 1.0
     */
    virtual std::string GetSessionId() = 0;

    /**
     * @brief Obtains the real-time playback position.
     *
     * @return Returns the real-time playback position, in ms.
     * @since 9
     * @version 1.0
     */
    virtual int64_t GetRealPlaybackPosition() = 0;
    
    /**
     * @brief Checks whether the controller is released.
     *
     * @return Returns <b>true</b> if the controller is released; returns <b>false</b> otherwise.
     * @since 9
     * @version 1.0
     */
    virtual bool IsDestroy() = 0;

    virtual ~AVSessionController() = default;
};
} // namespace OHOS::AVSession
/** @} */
#endif // OHOS_AVSESSION_CONTROLLER_H
