/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CAPI_INCLUDE_IPC_KIT_H
#define CAPI_INCLUDE_IPC_KIT_H

/**
 * @addtogroup IPCKit
 * @{
 *
 * @brief IPC头文件包含入口，方便开发者引用。
 *
 * @syscap SystemCapability.Communication.IPC.Core
 * @since 12
 */

/**
 * @file ipc_kit.h
 *
 * @brief IPC头文件包含入口，方便开发者引用。
 *
 * @library libipc_capi.so
 * @syscap SystemCapability.Communication.IPC.Core
 * @since 12
 */

#include "ipc_error_code.h"
#include "ipc_cparcel.h"
#include "ipc_cremote_object.h"
#include "ipc_cskeleton.h"

/** @} */
#endif
