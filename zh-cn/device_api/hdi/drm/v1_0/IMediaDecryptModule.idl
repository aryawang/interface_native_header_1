/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiDrm
 * @{

 * @brief DRM模块接口定义。
 *
 * DRM是数字版权管理，用于对多媒体内容加密，以便保护价值内容不被非法获取，
 * DRM模块接口定义了DRM实例管理、DRM会话管理、DRM内容解密的接口。
 *
 * @since 4.1
 * @version 1.0
 */

/**
 * @file IMediaDecryptModule.idl
 *
 * @brief 定义了DRM内容解密接口、解密模块实例释放接口。
 *
 * 模块包路径：ohos.hdi.drm.v1_0
 *
 * 引用：ohos.hdi.drm.v1_0.MediaKeySystemTypes
 *
 * @since 4.1
 * @version 1.0
 */


package ohos.hdi.drm.v1_0;

import ohos.hdi.drm.v1_0.MediaKeySystemTypes;

/**
*@brief 定义内容解密、解密模块实例释放函数。用于解密加密的内容。
*
* @since 4.1
* @version 1.0
*/
interface IMediaDecryptModule {
    /**
     * @brief 内容解密接口，该接口使用解密描述信息对源缓冲区数据解密
     * 并存放至目标缓冲区，提供安全内存和非安全内存两种类型的目标缓冲区。
     *
     * @param secure 是否在安全内存中解密，true表示使用安全内存，false表示使用非安内存。
     * @param cryptoInfo 密钥标识及数据加密的相关信息。
     * @param srcBuffer 待解密数据buffer。
       @param destBuffer 解密后数据buffer。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    DecryptMediaData([in] boolean secure, [in] struct CryptoInfo cryptoInfo, [in] struct DrmBuffer srcBuffer, [in] struct DrmBuffer destBuffer);

    /**
     * @brief 释放解密模块。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    Release();
};
/** @} */
