/**
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiHci
 * @{
 *
 * @brief HdiHci为HCI服务提供统一接口。
 *
 * 主机可以使用该模块提供的接口来初始化HCI（主机控制器接口），并通过该服务与控制器交换数据。
 *
 * @since 3.2
 */

/**
 * @file HciTypes.idl
 *
 * @brief 声明HCI模块使用的数据结构。
 *
 * 模块包路径：ohos.hdi.bluetooth.hci.v1_0
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.bluetooth.hci.v1_0;

/**
 * @brief 声明接口的操作结果。
 *
 * @since 3.2
 */
enum BtStatus {
    /** 成功。*/
    SUCCESS = 0,
    /** 初始化失败。*/
    INITIAL_ERROR = 1,
    /** 未知。*/
    UNKNOWN = 2,
};

/**
 * @brief 声明通过HCI传输的数据类型。
 *
 * @since 3.2
 */
enum BtType {
    /** HCI命令。*/
    HCI_CMD = 1,
    /** ACL数据。*/
    ACL_DATA = 2,
    /** SCO数据。*/
    SCO_DATA = 3,
    /** HCI事件。*/
    HCI_EVENT = 4,
    /** ISO数据。*/
    ISO_DATA = 5,
};
/** @} */