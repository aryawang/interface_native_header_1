/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 4.1
 * @version 1.2
 */

 /**
 * @file ICameraHostCallback.idl
 *
 * @brief ICameraHost的回调接口，提供Camera设备和闪关灯状态变化的回调函数，回调函数由调用者实现。
 *
 * 模块包路径：ohos.hdi.camera.v1_2
 *
 * 引用：
 * - ohos.hdi.camera.v1_0.ICameraHostCallback
 * - ohos.hdi.camera.v1_2.Types
 *
 * @since 4.1
 * @version 1.2
 */


package ohos.hdi.camera.v1_2;

import ohos.hdi.camera.v1_0.ICameraHostCallback;
import ohos.hdi.camera.v1_2.Types;

/**
 * @brief 定义Camera设备功能回调操作。
 *
 * 返回闪光灯状态
 *
 * @since 4.1
 * @version 1.2
 */
[callback] interface ICameraHostCallback extends ohos.hdi.camera.v1_0.ICameraHostCallback {
    /**
     * @brief 当闪光状态发生变化时调用以报告最新状态。
     *
     * @param status 闪光灯最新的状态。
     *
     * @since 4.1
     * @version 1.2
     */
    OnFlashlightStatusV1_2([in] enum FlashlightStatus status);
}
/** @} */