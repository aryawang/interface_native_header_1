/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiNfc
 * @{
 *
 * @brief 为nfc服务提供统一的访问nfc驱动的接口。
 *
 * NFC服务通过获取的nfc驱动对象提供的API接口访问nfc驱动，包括开关NFC、初始化NFC、读写数据、配置RF参数、
 * 通过IO控制发送NCI指令给nfc驱动。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file INfcInterface.idl
 *
 * @brief 定义NFC开关、初始化、传输数据的适配接口文件
 *
 * 模块包路径：ohos.hdi.nfc.v1_0
 *
 * 引用：
 * - ohos.hdi.nfc.v1_0.NfcTypes
 * - ohos.hdi.nfc.v1_0.INfcCallback
 *
 * @since 3.2
 * @version 1.0
 */
 
package ohos.hdi.nfc.v1_0;

import ohos.hdi.nfc.v1_0.NfcTypes;
import ohos.hdi.nfc.v1_0.INfcCallback;

/**
 * @brief 声明操作nfc芯片的API，包括关闭、打开nfc，初始化nfc，读写数据、配置RF参数、发送nci指令。
 *
 * @since 3.2
 * @version 1.0
 */

interface INfcInterface {
    /**
     * @brief 打开NFC，对NFC初始化。
     *
     * @param callbackObj NFC芯片发送给NFC协议栈的数据和事件的回调对象。
     * @return 操作成功返回0，否则返回失败。
     * 具体类型详见{@link NfcTypes}。
     *
     * @since 3.2
     * @version 1.0
     */
    Open([in] INfcCallback callbackObj, [out] enum NfcStatus status);

    /**
     * @brief NFC初始化。
     *
     * @param callbackObj NFC芯片发送给NFC协议栈的数据和事件的回调对象。
     * @return 操作成功返回0，否则返回失败。
     * 具体类型详见{@link NfcTypes}。
     *
     * @since 3.2
     * @version 1.0
     */
    CoreInitialized([in] List<unsigned char> data, [out] enum NfcStatus status);

    /**
     * @brief 启动RF discover之前对芯片进行预配置。
     *
     * @return 配置成功返回0，否则返回失败原因。
     * 具体类型详见{@link NfcTypes}。
     *
     * @since 3.2
     * @version 1.0
     */
    Prediscover([out] enum NfcStatus status);

    /**
     * @brief 发送数据给NFC控制器。
     * 
     * @param 待写入NFC控制器的数据。
     * @return 配置成功返回0，否则返回失败原因。
     * 具体类型详见{@link NfcTypes}。
     *
     * @since 3.2
     * @version 1.0
     */
    Write([in] List<unsigned char> data, [out] enum NfcStatus status);

    /**
     * @brief 允许HDF层发送NCI指令。
     * 
     * @return 配置成功返回0，否则返回失败原因。
     * 具体类型详见{@link NfcTypes}。
     *
     * @since 3.2
     * @version 1.0
     */
    ControlGranted([out] enum NfcStatus status);

    /**
     * @brief 周期性重启NFC。
     * 
     * @return 配置成功返回0，否则返回失败原因。
     * 具体类型详见{@link NfcTypes}。
     *
     * @since 3.2
     * @version 1.0
     */
    PowerCycle([out] enum NfcStatus status);

    /**
     * @brief 关闭NFC。
     * 
     * @return 配置成功返回0，否则返回失败原因。
     * 具体类型详见{@link NfcTypes}。
     *
     * @since 3.2
     * @version 1.0
     */
    Close([out] enum NfcStatus status);

    /**
     * @brief NFC协议栈通过IO控制指令和数据发送给HDI。
     * 
     * @param cmd NfcCommand中定义在控制指令，详见{@link NfcTypes}。
     * @param data 发送给HDI的数据。
     * @return 配置成功返回0，否则返回失败原因。
     * 具体类型详见{@link NfcTypes}。
     *
     * @since 3.2
     * @version 1.0
     */
    Ioctl([in] enum NfcCommand cmd, [in] List<unsigned char> data, [out] enum NfcStatus status);
}
/** @} */