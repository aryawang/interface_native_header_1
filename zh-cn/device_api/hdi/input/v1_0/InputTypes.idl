/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdiInput
 * @{
 *
 * @brief Input模块向上层服务提供了统一接口。
 *
 * 上层服务开发人员可根据Input模块提供的向上统一接口获取如下能力：Input设备的打开和关闭、Input事件获取、设备信息查询、回调函数注册、特性状态控制等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file InputTypes.idl
 *
 * @brief Input设备相关的数据类型定义。
 *
 * 本模块为Input服务定义了设备驱动接口所使用的结构体类型，包括设备信息、设备属性、设备能力等。
 *
 * 模块包路径：ohos.hdi.input.v1_0
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.input.v1_0;

/**
 * @brief Input设备描述信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct DevDesc {
    /** 设备索引 */
    unsigned int devIndex;

    /** 设备类型 */
    unsigned int devType;
};

/**
 * @brief Input设备的识别信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct DevIdentify {
    /** 总线类型 */
    unsigned short busType;

    /** 生产商编号 */
    unsigned short vendor;

    /** 产品编号 */
    unsigned short product;

    /** 版本号 */
    unsigned short version;
};

/**
 * @brief Input设备的维度信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct DimensionInfo {
    /** 坐标轴 */
    int axis;

    /** 记录各个坐标的最小值 */
    int min;

    /** 记录各个坐标的最大值 */
    int max;

    /** 记录各个坐标的分辨率 */
    int fuzz;

    /** 记录各个坐标的基准值 */
    int flat;

    /** 范围 */
    int range;
};

/**
 * @brief Input设备属性。
 *
 * @since 3.2
 * @version 1.0
 */
struct DevAttr {
    /** 设备名 */
    String devName;

    /** 设备识别信息 */
    struct DevIdentify id;

    /** 设备维度信息 */
    struct DimensionInfo[] axisInfo;
};

/**
 * @brief Input设备的能力属性，存储支持事件的位图。
 *
 * 用位的方式来表示该Input设备能够上报的事件类型。
 *
 *
 * @since 3.2
 * @version 1.0
 */
struct DevAbility {
    /** 设备属性 */
    unsigned long[] devProp;

    /** 用于记录支持的事件类型的位图 */
    unsigned long[] eventType;

    /** 记录支持的绝对坐标的位图 */
    unsigned long[] absCode;

    /** 记录支持的相对坐标的位图 */
    unsigned long[] relCode;

    /** 记录支持的按键值的位图 */
    unsigned long[] keyCode;

    /** 记录设备支持的指示灯的位图 */
    unsigned long[] ledCode;

    /** 记录设备支持的其他功能的位图 */
    unsigned long[] miscCode;

    /** 记录设备支持的声音或警报的位图 */
    unsigned long[] soundCode;

    /** 记录设备支持的作用力功能的位图 */
    unsigned long[] forceCode;

    /** 记录设备支持的开关功能的位图 */
    unsigned long[] switchCode;

    /** 按键类型的位图 */
    unsigned long[] keyType;

    /** LED类型的位图 */
    unsigned long[] ledType;

    /** 声音类型的位图 */
    unsigned long[] soundType;

    /** 开关类型的位图 */
    unsigned long[] switchType;
};

/**
 * @brief Input设备基础设备信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct DeviceInfo {
    /** 设备索引 */
    unsigned int devIndex;

    /** 设备类型 */
    unsigned int devType;

    /** 驱动芯片编码信息 */
    String chipInfo;

    /** 模组厂商名 */
    String vendorName;

    /** 驱动芯片型号 */
    String chipName;

    /** 设备属性 */
    struct DevAttr attrSet;

    /** 设备能力属性 */
    struct DevAbility abilitySet;
};

/**
 * @brief 扩展指令的数据结构。
 *
 * @since 3.2
 * @version 1.0
 */
struct ExtraCmd {
    /** 指令对应的编码 */
    String cmdCode;

    /** 指令传输的数据 */
    String cmdValue;
};

/**
 * @brief 热插拔事件数据包结构。
 *
 * @since 3.2
 * @version 1.0
 */
struct HotPlugEvent {
    /** 设备索引 */
    unsigned int devIndex;

    /** 设备类型 */
    unsigned int devType;

    /** 设备状态。
     *
     * - 1: 离线。
     * - 0: 在线。
     *
     */
    unsigned int status;
};

/**
 * @brief Input事件数据包结构。
 *
 * @since 3.2
 * @version 1.0
 */
struct EventPackage {
    /** 输入事件的属性 */
    unsigned int type;

    /** 输入事件的特定编码项 */
    unsigned int code;

    /** 输入事件编码项对应的值 */
    int value;

    /** 输入事件对应的时间戳 */
    unsigned long timestamp;
};
/** @} */
