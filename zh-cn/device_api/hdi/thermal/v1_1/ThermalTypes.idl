/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Thermal
 * @{
 *
 * @brief 提供设备温度管理、控制及订阅接口。
 *
 * 热模块为热服务提供的设备温度管理、控制及订阅接口。
 * 服务获取此模块的对象或代理后，可以调用相关的接口管理、控制和订阅设备温度。
 *
 * @since 3.1
 * @version 1.1
 */

/**
 * @file ThermalTypes.idl
 *
 * @brief 设备发热状态相关的数据类型。
 *
 * 热管理中使用的数据类型，包括设备发热的信息和设备发热的信息列表。
 *
 * 模块包路径：ohos.hdi.thermal.v1_1
 *
 * @since 3.1
 * @version 1.1
 */

package ohos.hdi.thermal.v1_1;

/**
 * @brief 设备发热的信息。
 *
 * @since 3.1
 */
struct ThermalZoneInfo {
    /** 发热器件的类型，不同设备支持的类型不同，可参考厂商提供的详细说明。 */   
    String type;
    /** 器件的温度值。 */
    int temp;
};

/**
 * @brief 设备发热的信息列表。
 *
 * @since 3.1
 */
struct HdfThermalCallbackInfo {
    /** 设备发热的信息列表。 */
    List<struct ThermalZoneInfo> info;
};
/** @} */
